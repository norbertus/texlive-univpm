% %%
%% This is file `univpm-beamer.cls',
%% _______________________________________________________________
%% 
%% The univpm-thesis class for typesetting the BSc or MSc
%% thesis of Università Politecnica delle Marche.
%% 
%% Copyright (C) 2012 by Andrea Claudi
%% 
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either
%% version 1.3 of this license or (at your option) any later
%% version. The latest version of this license is in
%% 
%%   http://www.latex-project.org/lppl.txt
%% 
%% and version 1.3 or later is part of all distributions of LaTeX
%% version 2005/12/01 or later.
%% 
%% This work has the LPPL maintenance status `author-maintained'.
%% 
%% The Current Maintainer of this work is:
%%   Andrea Claudi <a.claudi@univpm.it>
%% _______________________________________________________________
%% 
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{univpm-beamer}%
  [2011/20/10 v.1.0 Beamer template for Universita' Politecnica delle Marche]
\errorcontextlines=9

%% Carico ifthen e dichiaro i booleani per le opzioni
\RequirePackage{ifthen,etoolbox}

\newif\ifunivpm@coadv	\univpm@coadvfalse

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Carico la classe beamer
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\ProcessOptions\relax
\LoadClass{beamer}
\mode<presentation>{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Definisco lo stile per la presentazione
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Sopprimo i simboli di navigazione
\beamertemplatenavigationsymbolsempty

% Definisco i colori personalizzati
\definecolor{univpm-fg}{RGB}{122,0,11}
\definecolor{univpm-bg}{RGB}{244,236,236}

% Definisce il tema rotondo per gli elenchi puntati
\useinnertheme{rounded}

% Definisce i colori per i vari elementi della presentazione
\setbeamercolor{alerted text}{fg=red}
\setbeamercolor{block title}{bg=univpm-bg}
\setbeamercolor{block body}{fg=univpm-fg,bg=univpm-bg}
\setbeamercolor{fine separation line}{}
\setbeamercolor{frametitle}{fg=univpm-fg,bg=univpm-bg}
\setbeamercolor{item projected}{fg=white}
\setbeamercolor{normal text}{fg=black}
\setbeamercolor{separation line}{}
\setbeamercolor{structure}{fg=univpm-fg,bg=univpm-bg}
\setbeamercolor{title}{fg=univpm-fg,bg=univpm-bg}
\setbeamercolor{titlelike}{fg=univpm-fg,bg=univpm-bg}

% Blocchi con angoli arrotondati ed ombra
\setbeamertemplate{blocks}[rounded][shadow=true]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Definizione dei comandi della classe
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Nome dell'Università
\newcommand{\univpm}[1]{\def\univpm@univpm{#1}}
\univpm{Universit\`{a} Politecnica delle Marche}
%% Logo dell'Università
\def\univpm@logo{figures/logoUNIVPM}
%% Facoltà del corso di laurea
\newcommand{\thesisfaculty}[1]{\def\univpm@thesisfaculty{#1}}
%% Dipartimento a cui afferisce il relatore
\newcommand{\thesisdepartment}[1]{\def\univpm@thesisdepartment{#1}}
%% Nome del corso di laurea
\newcommand{\thesisprogramme}[1]{\def\univpm@thesisprogramme{#1}}
%% Tipo della tesi
\newcommand{\thesistype}[1]{\def\univpm@thesistype{#1}}
%% Titolo della tesi
\newcommand{\thesistitle}[1]{\def\univpm@thesistitle{#1}}
%% Autore della tesi
\newcommand{\thesisauthor}[1]{\def\univpm@thesisauthor{#1}}
%% Relatore della tesi
\newcommand{\thesisadvisor}[1]{\def\univpm@thesisadvisor{#1}}
%% Correlatore della tesi
\newcommand{\thesiscoadvisor}[1]{\def\univpm@thesiscoadvisor{#1} \univpm@coadvtrue}
%% Comando per la gestione dell'anno accademica
\newcommand{\thesisyear}[1]{\def\univpm@thesisyear{#1}}
%% Comando per l'immagine di background
\newcommand{\backgroundimage}[1]{\usebackgroundtemplate{\includegraphics[width=\paperwidth]{#1}}}
%% Comando per il logo dell'università
\newcommand{\unilogo}[1]{\pgfdeclareimage[height=1.5cm]{unilogo}{#1}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Definizione del frontespizio della presentazione
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title[Tesi di~\univpm@thesisauthor]{\univpm@thesistitle}
\date[]

\def
\maketitle{
  \begin{center}
    \pgfuseimage{unilogo}\\
    \footnotesize{\univpm@univpm\\\univpm@thesisfaculty\\
    \vspace{.1em}}
    \footnotesize{Corso di Laurea~\univpm@thesistype~in~\univpm@thesisprogramme}
    \vspace{-0.2em}
  \end{center}
  \begin{block}{\empty}
    \begin{center}
      \univpm@thesistitle     
    \end{center}
  \end{block}
  
  \vspace{1.5em}

  \small{\emph{Relatore}\hfill\emph{Candidato}}\\
  \small{\textbf{\univpm@thesisadvisor}\hfill\textbf{\univpm@thesisauthor}}\\
  \vspace{0.2em}

  \ifthenelse{\boolean{univpm@coadv}}{
    \emph{Correlatore}\\
    \textbf{\univpm@thesiscoadvisor}
  }{}

  \vspace{0.6em}

  \begin{center}
    \scriptsize{Anno Accademico \univpm@thesisyear}
  \end{center}
}
