% %%
%% This is file `univpm-exam.cls',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% univpm-thesis.dtx  (with options: `class')
%% _______________________________________________________________
%% 
%% The univpm-exam class for typesetting exam sheets
%% for Università Politecnica delle Marche, Ancona, Italy
%% 
%% Copyright (C) 2012 by Andrea Claudi
%% 
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either
%% version 1.3 of this license or (at your option) any later
%% version. The latest version of this license is in
%% 
%%   http://www.latex-project.org/lppl.txt
%% 
%% and version 1.3 or later is part of all distributions of LaTeX
%% version 2005/12/01 or later.
%% 
%% This work has the LPPL maintenance status `author-maintained'.
%% 
%% The Current Maintainer of this work is:
%%   Andrea Claudi <a.claudi@univpm.it>
%% _______________________________________________________________
%% 

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{univpm-exam}%
  [2012/24/02 v.1.0 Template for exam typesetting for Universita' Politecnica delle Marche]

%% Carico ifthen e dichiaro i booleani per le opzioni
\RequirePackage{ifthen,etoolbox}

\newif\ifunivpm@ans	\univpm@ansfalse
\newif\ifunivpm@warn	\univpm@warnfalse
\newif\ifunivpm@res	\univpm@resfalse

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Opzioni della classe
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\DeclareOption{ans}{\univpm@anstrue}
\DeclareOption{warn}{\univpm@warntrue}
\DeclareOption{evt}{\univpm@restrue}

\DeclareOption*{
  \ClassError{univpm-exam}%
    {L'opzione \CurrentOption\space non e' valida}%
    {Premi X per terminare e correggi}%
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Carico la classe exam
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\ProcessOptions\relax
\LoadClass[a4paper,10pt]{exam}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Ulteriori pacchetti richiesti
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\RequirePackage[italian]{babel}
\RequirePackage[utf8]{inputenc}
\RequirePackage{tikz}
\RequirePackage{graphicx}
\RequirePackage{listings}
\RequirePackage{courier}
\RequirePackage{color}
\RequirePackage{colortbl}
\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage[top=4cm, bottom=2.5cm, left=2cm, right=2.5cm]{geometry} 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Settaggi specifici per alcuni pacchetti
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Settings for the tikz library
\usetikzlibrary{backgrounds,calc}

% Settings for source listings
\lstset{
  language=C++,
  basicstyle=\ttfamily,
  keywordstyle=\bfseries\color{blue},
  commentstyle=\color{gray},
  stringstyle=\bfseries\color{red},
  showspaces=false,
  showstringspaces=false,
  showtabs=false,
  morekeywords={cout,cin,endl}
}

\lstnewenvironment{cppmessage}{\lstset{
  keywordstyle=\ttfamily,
  commentstyle=\ttfamily,
  stringstyle=\ttfamily
}}{}

\lstnewenvironment{pseudoc}{\lstset{language=C,mathescape=true}}{}
\lstnewenvironment{cc}{\lstset{language=C}}{}
\lstnewenvironment{sql}{\lstset{language=SQL}}{}
\lstnewenvironment{pseudo}{\lstset{mathescape=true}}{}

% Setting light colors for square papers
\definecolor{tbl-gray}{gray}{0.8}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Gestione delle opzioni
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Print the answers, if required to
\ifthenelse{\boolean{univpm@ans}}{\printanswers}{}

% Print warnings, if required to
\ifthenelse{\boolean{univpm@warn}}
{
  % Defines a new command to include exam warnings
  \newcommand{\includewarnings}[1]
  {\ifx\empty#1\empty\else \printexaminfo\input{#1}\printexamgreeting\fi}
}
{
  \newcommand{\includewarnings}[1]{\empty}
}

% Print evaluation table, if required to
\ifthenelse{\boolean{univpm@res}}
{
  % Defines a new command to include a grading table
  \newcommand{\includetable}[1]
  {\ifx\empty#1\empty\else 
    \printhorizontalrule{0.5cm}
    \begin{center}\textbf{\large \@examgradingtitle}\end{center}
    \begin{center}
      \gradetable[#1][questions] 
    \end{center}
  }  
}
{
  \newcommand{\includetable}[1]{\empty}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Definizione dei comandi della classe
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Defines a new command to set exam title
\newcommand{\examtitle}[1]
{\ifx\empty#1\empty\else\gdef\@examtitle{#1}\fi}%
\def\@examtitle{}

% Defines a new command to set teacher's name
\newcommand{\examprof}[1]
{\ifx\empty#1\empty\else\gdef\@examprof{#1}\fi}%
\def\@examprof{}

% Defines a new command to set teacher's department
\newcommand{\examdepartment}[1]
{\ifx\empty#1\empty\else\gdef\@examdepartment{#1}\fi}%
\def\@examdepartment{}

% Defines a new command to set exam date
\newcommand{\examdate}[1]
{\ifx\empty#1\empty\else\gdef\@examdate{#1}\fi}%
\def\@examdate{}

% Defines a new command to set exam duration
\newcommand{\examtime}[1]
{\ifx\empty#1\empty\else\gdef\@examtime{#1}\fi}%
\def\@examtime{}

% Defines a new command to set warnings header
\newcommand{\examinfo}[1]
{\ifx\empty#1\empty\else\gdef\@examinfo{#1}\fi}%
\def\@examinfo{}

% Defines a new command to set a greeting to the student
\newcommand{\examgreeting}[1]
{\ifx\empty#1\empty\else\gdef\@examgreeting{#1}\fi}%
\def\@examgreeting{}

% Defines a new command to set a greeting to the student
\newcommand{\examgradingtitle}[1]
{\ifx\empty#1\empty\else\gdef\@examgradingtitle{#1}\fi}%
\def\@examgradingtitle{}

% Defines a new command to print university's logo
\newcommand{\examlogo}[1]
{\ifx\empty#1\empty\else\gdef\@examlogo{\parbox{1cm}{\vspace{-2.9cm}\includegraphics[width=2.5cm]{#1}}}\fi}%
\def\@examlogo{}

% Defines a new command to print warnings header
\newcommand{\printexaminfo}[0]
{\begin{center}\textbf{\large \@examinfo}\end{center}}

% Defines a new command to print available time
\newcommand{\printexamtime}[0]
{\ifx\@examtime\empty\else \item Il tempo a disposizione è di \@examtime. \fi}

% Defines a new command to print a greeting to the student
\newcommand{\printexamgreeting}[0]
{\ifx\@examgreeting\empty\else \begin{flushright}\textbf{\@examgreeting}\end{flushright} }

% Defines a new command to print an horizontal rule
\newcommand{\printhorizontalrule}[1]
{\vspace{#1}\dotfill\vspace{#1}}

% Defines a new command to print a square paper
\newcommand{\squarepaper}[2]
{
  \setlength{\unitlength}{5mm}
  \begin{picture}(#2,#1)
  %\linethickness{0.05mm}
  \multiput(0,0)(1,0){#2}
    {\color{tbl-gray} \line(0,1){#1}}
  \multiput(0,0)(0,1){#1}
    {\color{tbl-gray} \line(1,0){#2}}
  \put(#2,0){\color{tbl-gray} \line(0,1){#1}}
  \put(0,#1){\color{tbl-gray} \line(1,0){#2}}
  \end{picture}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Stampa della pagina
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Defines the header
\header{}
{
  \centering \bfseries \Large \@examtitle\\
  \large \@examprof\\
  \normalsize \@examdate \\
  \vspace{0.5cm}
  \normalfont \parbox{\textwidth}{
    \makebox[0.1cm]{\enspace}
    \makebox[10cm]{Cognome e nome:\enspace\dotfill}
    \makebox[5cm]{Matricola:\enspace\dotfill}
  }
}
{\@examlogo}

% Defines the footer
\runningfooter{\@examdepartment}{}{\thepage\ / \numpages}

% Defines settings for the standard exam class
\addpoints
\qformat{{\bfseries Quesito \thequestion}~(\totalpoints~\points)~\hfill~Punteggio ottenuto: \dots/\totalpoints}
\pointpoints{punto}{punti}
\hqword{Quesito:}
\vqword{Quesito} 
\hpgword{Pg:}
\vpgword{Pagina}
\gradetablestretch{1.5}
\hpword{Max:}
\vpword{Max} 
\hsword{Punti:}
\vsword{Punti}
\htword{Tot.}
\renewcommand{\solutiontitle}{\noindent\textbf{Soluzione:}\par\noindent} 

\newlength\pageleft