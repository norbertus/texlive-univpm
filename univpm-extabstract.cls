% %%
%% This is file `univpm-extabstract.cls',
%% _______________________________________________________________
%% 
%% The univpm-extabstract class for typesetting the PhD thesis
%% extended summary for Università Politecnica delle Marche.
%% 
%% Copyright (C) 2013 by Andrea Claudi
%% 
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either
%% version 1.3 of this license or (at your option) any later
%% version. The latest version of this license is in
%% 
%%   http://www.latex-project.org/lppl.txt
%% 
%% and version 1.3 or later is part of all distributions of LaTeX
%% version 2005/12/01 or later.
%% 
%% This work has the LPPL maintenance status `author-maintained'.
%% 
%% The Current Maintainer of this work is:
%%   Andrea Claudi <a.claudi@univpm.it>
%% _______________________________________________________________
%% 
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{univpm-extabstract}%
  [2013/30/01 v.1.0 Template for PhD thesis extended abstracts of Universita' Politecnica delle Marche]

\RequirePackage{graphicx}
\RequirePackage{wrapfig}
\RequirePackage{color}
\RequirePackage{fancyhdr}
\RequirePackage{ifthen,etoolbox}
\setlength{\headheight}{\textwidth}

\definecolor{gray-univpm}{RGB}{216,216,216}

\LoadClass[12pt,%
           twoside=false,%
           cleardoublepage=empty,%
           chapterprefix=false]{scrbook}

\RequirePackage{hyperref}
\RequirePackage[top=2.5cm, bottom=4cm, left=2.5cm, right=2.5cm, headheight=40pt, voffset=1cm]{geometry}

% Smaller fonts
\setkomafont{chapter}{\normalfont\Large\sffamily\bfseries}
\addtokomafont{section}{\normalfont\large\sffamily\bfseries}
\addtokomafont{subsection}{\normalfont\normalsize\slshape}

% Macros for the document
\newcommand{\phdschool}[1]{\def\univpm@school{#1}}
\newcommand{\phdcurriculum}[1]{\def\univpm@curriculum{#1}}
\newcommand{\phdtitle}[1]{\def\univpm@title{#1}}
\newcommand{\phdauthor}[1]{\def\univpm@author{#1}}
\newcommand{\phdadvisor}[1]{\def\univpm@advisor{#1}}
\newcommand{\phdcoadvisor}[1]{\def\univpm@coadvisor{#1}}
\newcommand{\phddate}[1]{\def\univpm@date{#1}}
\newcommand{\univpm}[1]{\def\univpm@univpm{#1}}
\univpm{Universit\`{a} Politecnica delle Marche}
\newcommand{\phdlogo}[1]
{\ifx\empty#1\empty\else\gdef\univpm@logo{\includegraphics[width=1.6cm]{#1}}\fi}%
\def\univpm@logo{}
\def\univpm@authorlabel{Author:}
\def\univpm@advisorlabel{Tutor(s):}
\def\univpm@datelabel{Date:}

%% Comandi per la gestione dell'header e del footer
\fancyhf{}
\chead{\fancyplain{\footnotesize \univpm@author \\ {\let\\=\relax\footnotesize\slshape\univpm@title}\vspace{2mm}\hrule}{\footnotesize \univpm@author \\ {\let\\=\relax\footnotesize\slshape\univpm@title}\vspace{2mm}}}
\lfoot{\vspace{-1cm}
      \begin{wrapfigure}{l}{1.6cm}
	\vspace{-9mm}
	\includegraphics[width=1.4cm]{\univpm@logo}
      \end{wrapfigure} 
\footnotesize\univpm@school}
\cfoot{\vspace{-1.5cm}\hfill\rule{.90\textwidth}{.05mm}}
\rfoot{\vspace{-1cm}\footnotesize\thepage}

%% Comando per la gestione della prima pagina
\newcommand{\hscover}%
{%
  \begin{center}
    \colorbox{gray-univpm}{
    \begin{minipage}[t]{\textwidth}
      \hskip-5mm
      \vspace{-1mm}\hrule\vspace{4mm}
      \begin{wrapfigure}{l}{1.6cm}
	\vspace{-9mm}
	\univpm@logo
      \end{wrapfigure}
      \LARGE \univpm@school\par   
      \Large \univpm@univpm
      \vspace{4mm}\hrule\vspace{-1mm}
    \end{minipage}
    }
    \begin{minipage}[t]{\textwidth}
      \begin{center}
	\vspace{2cm}
	Extended summary\par
	\vspace{1.2cm}
	\Large \univpm@title\par
	\vspace{1cm}
	\small \textit{Curriculum: \univpm@curriculum}\par
	\vspace{1.5cm}
	\small \univpm@authorlabel\par
	\vspace{2.5mm}
	\large \univpm@author\par
	\vspace{5mm}
	\small \univpm@advisorlabel\par
	\vspace{2.5mm}
	\large \univpm@advisor\par
	\ifdefined\univpm@coadvisor
	  \vspace{2.5mm}
	  \large \univpm@coadvisor\par
	\fi
	\vspace{2cm}
	\small \univpm@datelabel~\univpm@date\par
	\vspace{1.5cm}
	\hskip4mm\rule{.95\textwidth}{0.05mm}
	\vspace{3mm}
      \end{center}
    \end{minipage}
    \end{center}
}

% \newenvironment{abstract}%
% { \textbf{Abstract.~~}  }
% {  }

\newenvironment{keywords}%
{ \textbf{Keywords.~} }
{\thispagestyle{empty}}

\newenvironment{abstract}
{ \textbf{Abstract.~} }
{ \vspace{1cm} }

%% Viene ridefinito il comando maketitle
\renewcommand*\maketitle{%
  \thispagestyle{empty}
  \hscover   %% Hardcover e pagina vuota
}

\renewcommand{\bibname}{References}

\endinput
%%
%% End of file `univpmextabstract.cls'.