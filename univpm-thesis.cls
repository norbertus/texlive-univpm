% %%
%% This is file `univpm-thesis.cls',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% univpm-thesis.dtx  (with options: `class')
%% _______________________________________________________________
%% 
%% The univpm-thesis class for typesetting the BSc or MSc
%% thesis of Università Politecnica delle Marche.
%% 
%% Copyright (C) 2012 by Andrea Claudi
%% 
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either
%% version 1.3 of this license or (at your option) any later
%% version. The latest version of this license is in
%% 
%%   http://www.latex-project.org/lppl.txt
%% 
%% and version 1.3 or later is part of all distributions of LaTeX
%% version 2005/12/01 or later.
%% 
%% This work has the LPPL maintenance status `author-maintained'.
%% 
%% The Current Maintainer of this work is:
%%   Andrea Claudi <a.claudi@univpm.it>
%% _______________________________________________________________
%% 
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{univpm-thesis}%
  [2011/20/10 v.1.0 Template for BSc and MSc thesis of Universita' Politecnica delle Marche]
\errorcontextlines=9

%% Carico ifthen e dichiaro i booleani per le opzioni
\RequirePackage{ifthen,etoolbox}

\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}

\newif\ifunivpm@aquattroprint \univpm@aquattroprintfalse
\newif\ifunivpm@english       \univpm@englishfalse
\newif\ifunivpm@italian       \univpm@italianfalse
\newif\ifunivpm@lof           \univpm@loffalse
\newif\ifunivpm@lot           \univpm@lotfalse

%% Dichiaro le opzioni
\DeclareOption{a4print}{\univpm@aquattroprinttrue}
\DeclareOption{english}{\univpm@englishtrue}
\DeclareOption{italian}{\univpm@italiantrue}
\DeclareOption{lof}{\univpm@loftrue}
\DeclareOption{lot}{\univpm@lottrue}

\DeclareOption*{
  \ClassError{univpm-thesis}%
    {L'opzione \CurrentOption\space non e' valida}%
    {Premi X per terminare e correggi}%
}

%% Carico la classe
\ProcessOptions\relax

\LoadClass[10pt,%
           twoside=true,%
           open=right,%
           cleardoublepage=empty,%
           chapterprefix=true]{scrbook}

%% Gestisco la selezione della lingua
\ifthenelse{\boolean{univpm@english}\AND%
            \boolean{univpm@italian}}%
{%
  \ClassError{univpm-thesis}%
    {Non puoi definire due lingue!}%
    {Premi X per terminare e correggi l'errore!}%
}%
{\relax}

\ifthenelse{\(\NOT\boolean{univpm@english}\)\AND%
            \(\NOT\boolean{univpm@italian}\)}%
{%
  \ClassError{univpm-thesis}%
    {Devi definire una lingua!}%
    {Premi X per terminare e correggi l'errore!}%
}%
{\relax}

\typeout{**************************************************}
\ifunivpm@english\typeout{* Hai selezionato: INGLESE}\fi
\ifunivpm@italian\typeout{* Hai selezionato: ITALIANO}\fi
\typeout{**************************************************}

\RequirePackage[english,italian]{babel}

\ifunivpm@english
  \AtBeginDocument{\selectlanguage{english}}
\fi

\ifunivpm@italian
  \AtBeginDocument{\selectlanguage{italian}}
  \RequirePackage{indentfirst}
\fi

\addto\captionsitalian{\def\acknowledgename{Ringraziamenti}}
\addto\captionsenglish{\def\acknowledgename{Acknowledgments}}

\addto\captionsitalian{\def\dedicationname{Dedica}}
\addto\captionsenglish{\def\dedicationname{Dedication}}

%% Richiedo il pacchetto per le immagini
\RequirePackage{graphicx}

%% Richiedo il pacchetto per l'interlinea
\RequirePackage{setspace}
\setstretch{1.15}

%% Imposto le dimensioni della pagina
\RequirePackage[twoside]{geometry}
\geometry{%
  papersize={210mm,297mm},
  text={160mm,237mm},
  marginratio=1:1,
  bindingoffset=4mm
}
\addtolength{\footskip}{-0.5cm}

%% Carico hyperref a seconda del driver usato
\RequirePackage{ifpdf}
\ifpdf
  \RequirePackage[pdfpagelabels]{hyperref}
\else
  \RequirePackage{hyperref}
\fi

\ifunivpm@aquattroprint%
  \RequirePackage[a4,
                  center,
                  cam]{crop}
\fi

%% Setup degli hyperlink
%% Il template genera link in nero (per non compromettere la stampa).
\hypersetup{
    colorlinks=true,       % false: boxed links; true: colored links
    linkcolor=black,          % color of internal links
    citecolor=black,        % color of links to bibliography
    filecolor=black,      % color of file links
    urlcolor=black           % color of external links
}

%% Città dell'autore
\newcommand{\thesislocation}[1]{\def\univpm@thesislocation{#1}}
%% Data di stampa (in qualsiasi formato)
\newcommand{\thesistime}[1]{\def\univpm@thesistime{#1}}

%% Definizione dei comandi per le label in inglese
\newcommand{\thesislabelEN}[1]{\def\univpm@thesislabelEN{#1}}
\newcommand{\departmentlabelEN}[1]{\def\univpm@departmentlabelEN{#1}}
\newcommand{\authorlabelEN}[1]{\def\univpm@authorlabelEN{#1}}
\newcommand{\advisorlabelEN}[1]{\def\univpm@advisorlabelEN{#1}}
\newcommand{\coadvisorlabelEN}[1]{\def\univpm@coadvisorlabelEN{#1}}
\newcommand{\curriculumadvisorlabelEN}[1]%
  {\def\univpm@curriculumadvisorlabelEN{#1}}
\newcommand{\directorlabelEN}[1]{\def\univpm@directorlabelEN{#1}}
\newcommand{\cyclelabelEN}[1]{\def\univpm@cyclelabelEN{#1}}
%% Label inglesi
\thesislabelEN{Degree in}
\departmentlabelEN{Department}
\authorlabelEN{Dissertation of:}
\advisorlabelEN{Advisor:}
\coadvisorlabelEN{Coadvisor:}
\cyclelabelEN{Academic Year}

%% Definizione dei comandi per le label in italiano
\newcommand{\thesislabelIT}[1]{\def\univpm@thesislabelIT{#1}}
\newcommand{\departmentlabelIT}[1]{\def\univpm@departmentlabelIT{#1}}
\newcommand{\authorlabelIT}[1]{\def\univpm@authorlabelIT{#1}}
\newcommand{\advisorlabelIT}[1]{\def\univpm@advisorlabelIT{#1}}
\newcommand{\coadvisorlabelIT}[1]{\def\univpm@coadvisorlabelIT{#1}}
\newcommand{\curriculumadvisorlabelIT}[1]%
  {\def\univpm@curriculumadvisorlabelIT{#1}}
\newcommand{\directorlabelIT}[1]{\def\univpm@directorlabelIT{#1}}
\newcommand{\cyclelabelIT}[1]{\def\univpm@cyclelabelIT{#1}}
%% Label italiane
\thesislabelIT{Corso di Laurea}
\departmentlabelIT{Dipartimento di}
\authorlabelIT{Tesi di:}
\advisorlabelIT{Relatore:}
\coadvisorlabelIT{Correlatore:}
\cyclelabelIT{Anno Accademico}

%% Definizione dei comandi per le label che verranno usate
\newcommand{\thesislabel}[1]{\def\univpm@thesislabel{#1}}
\newcommand{\authorlabel}[1]{\def\univpm@authorlabel{#1}}
\newcommand{\departmentlabel}[1]{\def\univpm@departmentlabel{#1}}
\newcommand{\advisorlabel}[1]{\def\univpm@advisorlabel{#1}}
\newcommand{\coadvisorlabel}[1]{\def\univpm@coadvisorlabel{#1}}
\newcommand{\curriculumadvisorlabel}[1]%
  {\def\univpm@curriculumadvisorlabel{#1}}
\newcommand{\directorlabel}[1]{\def\univpm@directorlabel{#1}}
\newcommand{\cyclelabel}[1]{\def\univpm@cyclelabel{#1}}

%% Scelgo la label giusta in base alla lingua selezionata
\ifunivpm@english%
  \thesislabel{\univpm@thesistype~\univpm@thesislabelEN~}
  \departmentlabel{\univpm@thesisdepartment~\univpm@departmentlabelEN}
  \authorlabel{\univpm@authorlabelEN}
  \advisorlabel{\univpm@advisorlabelEN}
  \coadvisorlabel{\univpm@coadvisorlabelEN}
  \curriculumadvisorlabel{\univpm@curriculumadvisorlabelEN}
  \directorlabel{\univpm@directorlabelEN}
  \cyclelabel{\univpm@cyclelabelEN~\univpm@thesisyear}
\fi

\ifunivpm@italian%
  \thesislabel{\univpm@thesislabelIT~\univpm@thesistype~in~}
  \departmentlabel{\univpm@departmentlabelIT~\univpm@thesisdepartment}
  \authorlabel{\univpm@authorlabelIT}
  \advisorlabel{\univpm@advisorlabelIT}
  \coadvisorlabel{\univpm@coadvisorlabelIT}
  \curriculumadvisorlabel{\univpm@curriculumadvisorlabelIT}
  \directorlabel{\univpm@directorlabelIT}
  \cyclelabel{\univpm@cyclelabelIT~\univpm@thesisyear}
\fi

%% Nome dell'Università
\newcommand{\univpm}[1]{\def\univpm@univpm{#1}}
\univpm{Universit\`{a} Politecnica delle Marche}
%% Indirizzo dell'Università
\newcommand{\facultyaddress}[1]{\def\univpm@facultyaddress{#1}}
\facultyaddress{Via Brecce Bianche -- 60131 Ancona (AN), Italy}
%% Logo dell'Università
\newcommand{\thesislogo}[1]
{\ifx\empty#1\empty\else\gdef\univpm@logo{\includegraphics[scale=.3]{#1}}\fi}%
\def\univpm@logo{}
%% Facoltà della scuola di dottorato
\newcommand{\thesisfaculty}[1]{\def\univpm@thesisfaculty{#1}}
%% Dipartimento a cui afferisce il relatore
\newcommand{\thesisdepartment}[1]{\def\univpm@thesisdepartment{#1}}
%% Nome del curriculum
\newcommand{\thesisprogramme}[1]{\def\univpm@thesisprogramme{#1}}
%% Tipo della tesi
\newcommand{\thesistype}[1]{\def\univpm@thesistype{#1}}
%% Titolo della tesi
\newcommand{\thesistitle}[1]{\def\univpm@thesistitle{#1}}
%% Sotto titolo della tesi
%\newcommand{\phdsubtitle}[1]{\def\univpm@phdsubtitle{#1}}
%% Autore della tesi
\newcommand{\thesisauthor}[1]{\def\univpm@thesisauthor{#1}}
%% Tutor del dottorando
\newcommand{\thesisadvisor}[1]{\def\univpm@thesisadvisor{#1}}
%% Co-Tutor del dottorando
\newcommand{\thesiscoadvisor}[1]{\def\univpm@thesiscoadvisor{#1}}
%% Comando per la gestione del ciclo di dottorato
\newcommand{\thesisyear}[1]{\def\univpm@thesisyear{#1}}
%% Comando per la gestione della dedica
\newcommand{\thesisdedication}[1]%
{%
  \def\univpm@dedication{#1}
}
%% Comando per la gestione dell'hardcover
\newcommand{\hscover}%
{%
%% Logo e nomi dell'Università
  \begin{minipage}[t]{\textwidth}
    \begin{center}
      \univpm@logo\par
      \small{
        \LARGE\textsc{\univpm@univpm}\par
	\large\textsc{\univpm@thesisfaculty}\par
	\large\textsc{\univpm@departmentlabel}\par
        \normalsize\textsc{\univpm@thesislabel\univpm@thesisprogramme}\par
      }
      \vspace{1em}
      \hrule
    \end{center}
  \end{minipage}

  \vspace{9em}

%% Titolo
  \begin{minipage}[t]{\textwidth}
    \begin{center}
      \huge\usekomafont{title}{\univpm@thesistitle}\par
    \end{center}
  \end{minipage}

  \vfill

%% Autore, tutor e cordinatore
  \begin{minipage}[t]{\textwidth}
    \begin{flushright}
      \large{%
        \univpm@authorlabel\par
      \textbf{\univpm@thesisauthor}\par
      }
    \end{flushright}
    \vspace{1em}
    \univpm@advisorlabel\par
    \textbf{\univpm@thesisadvisor}\par
    \vspace{2em}
    \ifdefined\univpm@thesiscoadvisor
      \univpm@coadvisorlabel\par
      \textbf{\univpm@thesiscoadvisor}\par
      \vspace{2em}
    \fi
  \end{minipage}

  \vspace{2em}

%% Anno accademico
  \begin{minipage}[t]{\textwidth}
    \begin{center}
      \univpm@cyclelabel\par
    \end{center}
  \end{minipage}
}

%% Comando per la gestione del colophon
\newcommand{\colophon}%
{%
  \hfill\vfill
  \noindent\begin{minipage}[t]{\textwidth}
    \hrule
    \vspace{1em}
    \begin{center}
      \small{
	\textsc{\univpm@departmentlabel}\par
        \textsc{\univpm@univpm}\par
        \textsc{\univpm@thesisfaculty}\par
        \univpm@facultyaddress\par
      }
    \end{center}
  \end{minipage}
}

%% Viene ridefinito il comando maketitle
\renewcommand*\maketitle{%
%% Hardcover e pagina vuota
  \ifpdf\pdfbookmark{Hardcover}{Hardcover}\fi
  \hscover
  \cleardoublepage\thispagestyle{empty}
%% Softcover e colophon
  \ifpdf\pdfbookmark{Softcover}{Softcover}\fi
  \hscover
  \clearpage\thispagestyle{empty}
  \colophon
%% Dedica, se presente
  \ifdefined\univpm@dedication
    \cleardoublepage\thispagestyle{empty}
    \ifpdf\pdfbookmark{\dedicationname}{\dedicationname}\fi
    \hfill\vfill
    \begin{flushright}
      \large\textit{\univpm@dedication}
    \end{flushright}
    \vfill
  \fi
}
%% Ambiente per gestire i ringraziamenti
\newenvironment{thesisacknowledge}[1][]%
{%
%% In base alla lingua selezionata creo titolo e testo
%%  \selectlanguage{#1}
  \ifblank{#1}{}{\begin{otherlanguage}{#1}}
    \chapter*{\acknowledgename}
    \ifpdf
      \pdfbookmark{\acknowledgename}{\acknowledgename}
    \fi
  \ifblank{#1}{}{\end{otherlanguage}}
}%
{%
%% Stampo data e firma dell'autore
    \ifdefined\univpm@thesislocation
      \ifdefined\univpm@thesistime
        \bigskip\par
        \noindent\textit{\univpm@thesislocation, \univpm@thesistime}\par
        \hfill\univpm@thesisauthor\par
      \else
        \ClassError{univpm-thesis}%
          {Devi definire la data di stampa!}%
          {Premi X per terminare e correggi l'errore!}%
      \fi
    \else
      \ClassError{univpm-thesis}%
        {Devi definire il luogo di stampa!}%
        {Premi X per terminare e correggi l'errore!}%
    \fi
%% Ripristino la lingua
%%  \ifunivpm@english
%%    \selectlanguage{english}
%%  \fi

%%  \ifunivpm@italian
%%    \selectlanguage{italian}
%%  \fi
}
%% Ambiente per gestire l'abstract
\newenvironment{thesisabstract}[1][]%
{%
%% In base alla lingua selezionata creo titolo e testo
%%  \selectlanguage{#1}
  \ifblank{#1}{}{\begin{otherlanguage}{#1}}
    \chapter*{\abstractname}
    \ifpdf\pdfbookmark{\abstractname}{\abstractname}\fi
  \ifblank{#1}{}{\end{otherlanguage}}
}
{%
%% Ripristino la lingua
%%  \ifunivpm@english
%%    \selectlanguage{english}
%%  \fi

%%  \ifunivpm@italian
%%    \selectlanguage{italian}
%%  \fi
\relax}
%% Ambiente per gestire i ringraziamenti
\newcommand{\thesistoc}%
{%
  \cleardoublepage
  \ifpdf\pdfbookmark{\contentsname}{\contentsname}\fi
  \tableofcontents

  \ifunivpm@lof%
    \cleardoublepage
    \ifpdf\pdfbookmark{\listfigurename}{\listfigurename}\fi
    \listoffigures
  \fi

  \ifunivpm@lot%
    \cleardoublepage
    \ifpdf\pdfbookmark{\listtablename}{\listtablename}\fi
    \listoftables
  \fi
}

\endinput
%%
%% End of file `univpm-thesis.cls'.